<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class CottageRegisterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('bathrooms', TextType::class, ['label' => 'Введите количество ванных комнат'])
            ->add('bathrobes', TextType::class, ['label' => 'Количество халатов',
                                                             'attr' => ['pattern' => "\d+"]
                                                            ])
            ->add('save', SubmitType::class, ['label' => 'Зарегистрировать',
                                                          'attr' => ['class' => 'btn btn-primary']]);
    }
}
