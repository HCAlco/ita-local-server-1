<?php
/**
 * Created by PhpStorm.
 * User: aibek
 * Date: 13.06.18
 * Time: 15:50
 */

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('max', IntegerType::class, ['label' => 'Максимальная цена',
                                                          'attr' => [
                                                              'pattern' => '\d+'
                                                          ],
                                                          'required' => false])
            ->add('title', TextType::class, ['label' => 'Название', 'required' => false])
            ->add('min', IntegerType::class, ['label' => 'Минимальная цена',
                                                          'attr' => [
                                                            'pattern' => '\d+'
                                                          ],
                                                         'required' => false])
            ->add('type', ChoiceType::class, [
                'label' => ' ',
                'placeholder' => 'Выберите тип объекта',
                'choices' => [
                    'Пансионат' => 'pension',
                    'Коттедж' => 'cottage'
                ],
                'required' => false,

            ])
            ->add('save', SubmitType::class, ['label' => 'Поиск',
                                                          'attr' => array('class' => 'btn btn-primary')]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
    }
}