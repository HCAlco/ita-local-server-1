<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class HabitatRegisterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type', ChoiceType::class, [
                'label' => 'Тип объекта',
                'placeholder' => 'Выберите тип объекта',
                'choices' => [
                    'Пансионат' => 'pension',
                    'Коттедж' => 'cottage'
                ],

            ])
            ->add('title', TextType::class, ['label' => 'Название'])
            ->add('roomqty', TextType::class, [
                                                            'label' => 'Количество номеров (комнат)',
                                                            'attr' => ['pattern' => "\d+"]
                                                           ])
            ->add('owner', TextType::class, ['label' => 'Контактное лицо'])
            ->add('phone', TextType::class, ['label' => 'Контактный телефон',
                                                         'attr' => ['pattern' => "0[57]{1}\d{2} \d{3} \d{3}",
                                                                    'maxlength' => 12,
                                                                    'placeholder' => '0555 123 456']])
            ->add('address', TextType::class, ['label' => 'Адрес объекта',
                                                           'attr' => ['placeholder' => "Выберите на карте местопложение"],
                                                           'required' => 'required'])
            ->add('price', TextType::class, ['label' => 'Цена за сутки (в сомах)',
                                                         'attr' => ['pattern' => "\d+",
                                                                    'placeholder' => '1200']])
            ->add('save', SubmitType::class, ['label' => 'Продолжить',
                                                          'attr' => ['class' => 'btn btn-primary']]);
    }
}
