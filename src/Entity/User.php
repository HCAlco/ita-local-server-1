<?php

namespace App\Entity;

use DateTimeZone;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements UserInterface
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=1024)
     */
    private $roles;

    /**
     * @ORM\Column(type="string", length=254, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=1024)
     */
    private $password;


    /**
     * @ORM\Column(type="string", length=254, unique=true)
     */
    private $passport;

    /**
     * @ORM\Column(type="string", length=254, nullable=true, unique=true)
     */
    private $google;

    /**
     * @ORM\Column(type="string", length=254, nullable=true, unique=true)
     */
    private $facebook;

    /**
     * @ORM\Column(type="string", length=254, nullable=true, unique=true)
     */
    private $vkontakte;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $registeredAt;

    public function __construct()
    {
        $this->registeredAt = new \DateTime("now", new DateTimeZone('Asia/Bishkek'));
        $this->roles = json_encode(['ROLE_USER']);
    }

    /**
     * @return \DateTime
     */
    public function getRegisteredAt(): ?\DateTime
    {
        return $this->registeredAt;
    }

    /**
     * @param \DateTime $registeredAt
     */
    public function setRegisteredAt(\DateTime $registeredAt): void
    {
        $this->registeredAt = $registeredAt;
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     *
//     * @return (Role|string)[] The user roles
     */
    public function getRoles()
    {
        return json_decode($this->roles, true);
    }

    public function addRole(string $role)
    {
        $roles = json_decode($this->roles, true);
        $roles[] = $role;
        $this->roles = json_encode($roles);
    }

    public function setRoles(array $roles)
    {
        $this->roles = json_encode($roles);
    }

    /**
     * Returns the password used to authenticate the user.
     *
     * This should be the encoded password. On authentication, a plain-text
     * password will be salted, encoded, and then compared to this value.
     *
     * @return string The password
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Returns the salt that was originally used to encode the password.
     *
     * This can return null if the password was not encoded using a salt.
     *
     * @return string|null The salt
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials()
    {
    }

    /**
     * @param mixed $password
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @param mixed $email
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $passport
     * @return User
     */
    public function setPassport($passport)
    {
        $this->passport = $passport;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPassport()
    {
        return $this->passport;
    }

    /**
     * @param string $username
     * @return User
     */
    public function setUsername(string $username): User
    {
        $this->email = $username;
        return $this;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->email;
    }

    /**
     * @return array
     */
    public function toArray(){
        return [
            'email' => $this->email,
            'password' => $this->password,
            'passport' => $this->passport,
            'roles' => $this->getRoles(),
            'google' => $this->google,
            'facebook' => $this->facebook,
            'vkontakte' => $this->vkontakte,
        ];
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->roles[0];
    }

    /**
     * @param mixed $google
     * @return User
     */
    public function setGoogle($google)
    {
        $this->google = $google;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getGoogle()
    {
        return $this->google;
    }

    /**
     * @param mixed $facebook
     * @return User
     */
    public function setFacebook($facebook)
    {
        $this->facebook = $facebook;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFacebook()
    {
        return $this->facebook;
    }

    /**
     * @param mixed $vkontakte
     * @return User
     */
    public function setVkontakte($vkontakte)
    {
        $this->vkontakte = $vkontakte;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getVkontakte()
    {
        return $this->vkontakte;
    }

    /**
     * @param $network
     * @param $id
     * @return $this
     */
    public function setSocialUId($network, $id){
        switch ($network){
            case ('google'):
                $this->google = $id;
                break;
            case ('vkontakte'):
                $this->vkontakte = $id;
                break;
            case ('facebook'):
                $this->facebook = $id;
                break;
        }
        return $this;
    }
}
