<?php

namespace App\Controller;

use App\Form\FilterType;
use App\Model\Api\ApiContext;
use App\Model\Api\ApiException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BasicController extends Controller
{
    /**
     * @Route("/", name="app_homepage")
     * @param Request $request
     * @param ApiContext $apiContext
     * @return Response
     * @throws ApiException
     */
    public function indexAction(Request $request,ApiContext $apiContext)
    {
        $error = '';
        $form = $this->createForm(FilterType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $data = $form->getData();
                $habitats = $apiContext->getHabitatsByFilters($data);

                return $this->render('Index/index.html.twig', [
                    'form' => $form->createView(),
                    'habitats' => $habitats
                ]);

            }catch (ApiException $e){
                $error = 'Error: Что-то не так: '.$e->getMessage();
            }
        }
        $habitats = $apiContext->getAllHabitatInfo();
        return $this->render('Index/index.html.twig', [
            'form' => $form->createView(),
            'habitats' => $habitats,
            'error' => $error
        ]);
    }
}
