<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\LoginType;
use App\Form\RegisterType;
use App\Model\Api\ApiContext;
use App\Model\Api\ApiException;
use App\Model\User\UserHandler;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends Controller
{
    /**
     * @Route("/register", name="app_register")
     * @param Request $request
     * @param ApiContext $context
     * @param UserHandler $userHandler
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function registerAction(Request $request, ApiContext $context, UserHandler $userHandler)
    {
        $error = '';
        $user = new User();
        $form = $this->createForm(RegisterType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $user = $form->getData();
                if ($context->clientExists($user->getEmail(), $user->getPassport())) {
                    $error = 'Клиент с таким именем уже существует. Перейдите на страницу авторизации';
                } else {
                    $data = $user->toArray();
                    $context->createClient($data);
                    $user = $userHandler->createNewUser($data);

                    $em = $this->getDoctrine()->getManager();
                    $em->persist($user);
                    $em->flush();
                    return $this->redirectToRoute('app_login');
                }
            }catch (ApiException $e){
                $error = 'Error: Что-то не так: '.$e->getMessage();
            }
        }
        return $this->render('Index/register.html.twig', array(
            'form' => $form->createView(),
            'error' => $error
        ));
    }

    /**
     * @Route("/login", name="app_login")
     * @param Request $request
     * @param ApiContext $context
     * @param UserHandler $userHandler
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     * @throws ApiException
     */
    public function loginAction(Request $request, ApiContext $context, UserHandler $userHandler)
    {
        $error = '';
        $user = new User();
        $form = $this->createForm(LoginType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $local_user = $this
                ->getDoctrine()
                ->getRepository('App:User')
                ->findOneByEmailAndPassword($user->getEmail(), $userHandler->encodePlainPassword($user->getPassword()));
            if($local_user){
                $userHandler->putUserInSession($local_user);
                return $this->redirectToRoute('app_homepage');
            }
            try {
                $api_data = $context->checkClientCredentials($user->getEmail(), $userHandler->encodePlainPassword($user->getPassword()));
                if ($api_data) {
                    /** @var array $api_data */
                    $user = $userHandler->createNewUser($api_data, false);
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($user);
                    $em->flush();
                    $userHandler->putUserInSession($user);
                    return $this->redirectToRoute('app_homepage');
                } else {
                    $error = 'Введены неправильные данные';
                }
            }catch (ApiException $e){
                $error = $e->getMessage();
            }
        }
        return $this->render('Index/login.html.twig', array(
            'form' => $form->createView(),
            'error' => $error
        ));
    }

    /**
     * @Route("/profile/{id}", name="app_profile")
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function profileAction(int $id)
    {
        $social = [];
        $user = $this->getDoctrine()->getRepository('App:User')->find($id);
        $social['google'] = $this->getUser()->getGoogle() ?? 'no';
        $social['facebook'] = $this->getUser()->getFacebook() ?? 'no';
        $social['vkontakte'] = $this->getUser()->getVkontakte() ?? 'no';

        return $this->render('Index/profile.html.twig', array(
            'user' => $user,
            'social' => $social
        ));
    }

    /**
     * @Route("/login/{afterDenied}", name="login")
     * @param null/string $afterDenied
     * @return Response
     */
    public function accessDeniedAction($afterDenied = null)
    {

        if ($afterDenied) {
            return new Response('Ошибка - доступа нет');
        }

        return new Response('доступа есть');
    }

}
