<?php

namespace App\Controller;

use App\Form\CottageRegisterType;
use App\Form\HabitatRegisterType;
use App\Form\PensionRegisterType;
use App\Model\Api\ApiContext;
use App\Model\Api\ApiException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/habitat")
 */
class HabitatController extends Controller
{
    /**
     * @Route("/register", name="app_habitat_register")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function register1Action(Request $request)
    {
        $error = '';
        $form = $this->createForm(HabitatRegisterType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $habitat = $form->getData();
            return $this->redirectToRoute('app_habitat_register_2', ['data' => $habitat]);
        }
        return $this->render('Habitat/register_habitat.html.twig', array(
            'form' => $form->createView(),
            'error' => $error
        ));
    }

    /**
     * @Route("/register/extra", name="app_habitat_register_2")
     * @param Request $request
     * @param ApiContext $apiContext
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function register2Action(
        Request $request,
        ApiContext $apiContext
    )
    {
        $error = '';
        $first_reg_data = $request->query->get('data');
        dump($first_reg_data);
        if($first_reg_data['type'] == 'pension'){
            $form = $this->createForm(PensionRegisterType::class);
        }elseif($first_reg_data['type'] == 'cottage'){
            $form = $this->createForm(CottageRegisterType::class);
        }
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $second_reg_data = $form->getData();
                $apiContext->createHabitat(array_merge($first_reg_data, $second_reg_data));
                return $this->redirectToRoute('app_homepage', ['data' => $second_reg_data]);

            }catch (ApiException $e){
                $error = 'Error: Что-то не так: '.$e->getMessage();
            }
        }
        return $this->render('Habitat/extra_register_habitat.html.twig', array(
            'form' => $form->createView(),
            'error' => $error
        ));
    }

    /**
     * @Route("/view/{title}", name="app_habitat_view")
     * @param string $title
     * @param ApiContext $apiContext
     * @return JsonResponse|\Symfony\Component\HttpFoundation\RedirectResponse|Response
     * @throws ApiException
     */
    public function viewHabitatAction(
        string $title,
        ApiContext $apiContext
    )
    {
        $habitat = $apiContext->getHabitatsByFilters(['title' => $title]);
        $booked = $apiContext->getBookedHabitatsByHabitatId(['habitat_id' => $habitat[0]['id']]);
        dump($booked);
        return $this->render('Habitat/view.html.twig', array(
            'habitat' => $habitat[0],
            'booked' => $booked
        ));
    }

    /**
     * @Route("/booked/get", name="app_habitat_get_booked_habitats")
     * @param Request $request
     * @param ApiContext $apiContext
     * @return JsonResponse|\Symfony\Component\HttpFoundation\RedirectResponse|Response
     * @throws ApiException
     */
    public function getBookedHabitatsByIdAction(
        Request $request,
        ApiContext $apiContext
    )
    {
        $booked = $apiContext->getBookedHabitatsByHabitatId(['habitat_id' => $request->get('habitat_id')]);
        return new JsonResponse($booked);
    }

    /**
     * @Route("/booking", name="app_habitat_booking")
     * @param Request $request
     * @param ApiContext $apiContext
     * @return JsonResponse|\Symfony\Component\HttpFoundation\RedirectResponse|Response
     * @throws ApiException
     */
    public function habitatBookingAction(
        Request $request,
        ApiContext $apiContext
    )
    {
        if(empty($this->getUser()) || in_array('ROLE_LANDLORD', $this->getUser()->getRoles())){
            return new JsonResponse('Access Denied');
        }
        $result = '';
        $api_response = $apiContext->checkBookedHabitatsByUser(['email' => $this->getUser()->getEmail()]);
        if($api_response){
            $result = 'Exists';
        }else{
            $data['tenant'] = $this->getUser()->getEmail();
            $data['number'] = $request->get('number');
            $data['habitat'] = $request->get('habitat');
            $result = $apiContext->bookingHabitat($data);
        }
        return new JsonResponse($result);
    }
}
