<?php

namespace App\Controller;

use App\Entity\User;
use App\Model\Api\ApiContext;
use App\Model\Api\ApiException;
use App\Model\User\UserHandler;
use App\Repository\UserRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ULoginController extends Controller
{
    const ULOGIN_DATA = 'ulogin-data';
    /**
     * @Route("/social_auth", name="app_social_auth")
     * @param ObjectManager $manager
     * @param UserRepository $userRepository
     * @param UserHandler $userHandler
     * @param ApiContext $context
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function socialAuthAction(
        ObjectManager $manager,
        UserRepository $userRepository,
        UserHandler $userHandler,
        ApiContext $context
    )
    {

        $s = file_get_contents('http://ulogin.ru/token.php?token=' . $_POST['token'] . '&host=' . $_SERVER['HTTP_HOST']);
        $social_user = json_decode($s, true);

        $user = $this->getUser();
        if($user){
            $temp = $userRepository->findOneByGoogleFacebookOrVk($social_user['uid']);
            if($temp){
                $temp->setSocialUId($social_user['network'], null);
                $manager->persist($temp);
                $manager->flush();
            }
            $user->setSocialUId($social_user['network'], $social_user['uid']);
            $manager->persist($user);
            $manager->flush();
            return $this->render('Index/profile.html.twig', array(
                'user' => $user,
                'social' => $userHandler->getSocialInfo($user)
            ));
        }
        $error = '';
        $user = $userRepository->findOneByGoogleFacebookOrVk($social_user['uid']);
        if($user){
            $userHandler->putUserInSession($user);
            return $this->render('Index/profile.html.twig', array(
                'user' => $user,
                'social' => $userHandler->getSocialInfo($user)
            ));
        }else {
            try {
                $api_data = $context->getInfoBySocialId($social_user['uid']);
                if ($api_data) {
                    $user = $userRepository->findOneByEmailAndPassport($api_data['email'], $api_data['passport']);
                    if(!$user){
                        $user = $userHandler->createNewUser($api_data);
                    }
                    $user->setGoogle($api_data['google']);
                    $user->setVkontakte($api_data['vkontakte']);
                    $user->setFacebook($api_data['facebook']);
                    $manager->persist($user);
                    $manager->flush();

                    $userHandler->putUserInSession($user);
                    return $this->render('Index/profile.html.twig', array(
                        'user' => $user,
                        'social' => $userHandler->getSocialInfo($user)
                    ));

                }else{
                    $error = 'Вы ещё не регистрировались у нас, либо не привязали данную соц сеть к аккаунту';
                }
            }catch (ApiException $e){
                $error = $e->getMessage();
            }
        }
        $user = new User();
        $user->setEmail($social_user['email']);
        $form = $this->createForm('App\Form\ULoginRegisterType', $user,
            ['action' => $this
                ->get('router')
                ->generate('app_social_auth_2')
            ]);
        $this->get('session')->set(self::ULOGIN_DATA, $s);


        return $this->render('Index/social.html.twig', array(
            'user' => $social_user,
            'error' => $error,
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/social_auth_2", name="app_social_auth_2")
     * @param Request $request
     * @param UserHandler $userHandler
     * @param ApiContext $context
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function socialAuth2Action(
        Request $request,
        UserHandler $userHandler,
        ApiContext $context
    )
    {
        $error = '';
        $social_user = json_decode($this->get('session')->get(self::ULOGIN_DATA), true);
        $user = new User();
        $form = $this->createForm('App\Form\ULoginRegisterType',
            $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                if ($context->clientExists($user->getEmail(), $user->getPassport())) {
                    $error = 'Клиент с таким именем уже существует. Перейдите на страницу авторизации';
                } else {
                    $user->setPassword('social_user'.time());
                    $user->setSocialUId($social_user['network'], $social_user['uid']);
                    $data = $user->toArray();
                    $context->createClient($data);
                    $user = $userHandler->createNewUser($data);

                    $em = $this->getDoctrine()->getManager();
                    $em->persist($user);
                    $em->flush();
                    $userHandler->putUserInSession($user);
                    return $this->redirectToRoute('app_homepage');
                }
            }catch (ApiException $e){
                $error = 'Error: ' . $e->getMessage().'  |||  '.var_export($e->getResponse(),1);
            }
        }
        return $this->render('Index/social.html.twig', array(
            'user' => $social_user,
            'error' => $error,
            'form' => $form->createView()
        ));
    }
}
