<?php

namespace App\Model\Api;


class ApiContext extends AbstractApiContext
{
    const ENDPOINT_PING = '/ping';
    const ENDPOINT_REGISTER = '/register';
    const ENDPOINT_CLIENT_EXISTS = '/check/{email}/{passport}';
    const ENDPOINT_CLIENT_PASSWORD_ENCODE = '/client/password/encode';
    const ENDPOINT_CHECK_CLIENT_CREDENTIALS = '/credentials/{email}/{password}';
    const ENDPOINT_CHECK_AUTH_ID = '/auth/';
    const ENDPOINT_HABITAT_REGISTER = '/habitat/register';
    const ENDPOINT_HABITAT_GET_INFO = '/habitat/getinfo';
    const ENDPOINT_HABITAT_GET_INFO_BY_FILTERS = '/habitat/filters';
    const ENDPOINT_HABITAT_CHECK_BOOKED_BY_USER = '/habitat/check';
    const ENDPOINT_HABITAT_GET_BOOKED = '/habitat/booked/get';
    const ENDPOINT_HABITAT_BOOKING = '/habitat/booking';

    /**
     * @return mixed
     * @throws ApiException
     */
    public function makePing()
    {
        return $this->makeQuery(self::ENDPOINT_PING, self::METHOD_GET);
    }

    /**
     * @param string $passport
     * @param string $email
     * @return bool
     * @throws ApiException
     */
    public function clientExists($email, $passport = null)
    {
        $endPoint = $this->generateApiUrl(
            self::ENDPOINT_CLIENT_EXISTS, [
            'email' => $email,
            'passport' => $passport
        ]);

        return $this->makeQuery($endPoint, self::METHOD_HEAD);
    }

    /**
     * @param string $password
     * @param string $email
     * @return bool
     * @throws ApiException
     */
    public function checkClientCredentials($email, $password)
    {
        $endPoint = $this->generateApiUrl(
            self::ENDPOINT_CHECK_CLIENT_CREDENTIALS, [
            'email' => $email,
            'password' => $password
        ]);

        return $this->makeQuery($endPoint, self::METHOD_GET);

    }

    /**
     * @param array $data
     * @return mixed
     * @throws ApiException
     */
    public function createClient(array $data)
    {
        return $this->makeQuery(self::ENDPOINT_REGISTER, self::METHOD_POST, $data);
    }

    /**
     * @param array $data
     * @return mixed
     * @throws ApiException
     */
    public function createHabitat(array $data)
    {
        return $this->makeQuery(self::ENDPOINT_HABITAT_REGISTER, self::METHOD_POST, $data);
    }

    /**
     * @return mixed
     * @throws ApiException
     */
    public function getAllHabitatInfo()
    {
        return $this->makeQuery(self::ENDPOINT_HABITAT_GET_INFO, self::METHOD_GET);
    }

    /**
     * @param array $data
     * @return mixed
     * @throws ApiException
     */
    public function getHabitatsByFilters(array $data)
    {
        return $this->makeQuery(self::ENDPOINT_HABITAT_GET_INFO_BY_FILTERS, self::METHOD_GET, $data);
    }

    /**
     * @param array $data
     * @return mixed
     * @throws ApiException
     */
    public function getBookedHabitatsByHabitatId(array $data)
    {
        return $this->makeQuery(self::ENDPOINT_HABITAT_GET_BOOKED, self::METHOD_GET, $data);
    }

    /**
     * @param string $id
     * @return mixed
     * @throws ApiException
     */
    public function getInfoBySocialId($id)
    {
        return $this->makeQuery(self::ENDPOINT_CHECK_AUTH_ID.$id, self::METHOD_GET);
    }

    /**
     * @param array $data
     * @return mixed
     * @throws ApiException
     */
    public function checkBookedHabitatsByUser(array $data)
    {
        return $this->makeQuery(self::ENDPOINT_HABITAT_CHECK_BOOKED_BY_USER, self::METHOD_HEAD, $data);
    }

    /**
     * @param array $data
     * @return mixed
     * @throws ApiException
     */
    public function bookingHabitat(array $data)
    {
        return $this->makeQuery(self::ENDPOINT_HABITAT_BOOKING, self::METHOD_GET, $data);
    }

    /**
     * @param string $password
     * @return mixed
     * @throws ApiException
     */
    public function encodePassword(string $password)
    {
        $response = $this->makeQuery(self::ENDPOINT_CLIENT_PASSWORD_ENCODE, self::METHOD_GET, [
            'plainPassword' => $password
        ]);

        return $response['result'];
    }
}
