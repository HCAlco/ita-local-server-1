<?php

namespace App\Model\User;

use App\Entity\Landlord;
use App\Entity\Tenant;
use App\Entity\User;
use App\Model\Api\ApiContext;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class UserHandler
{
    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @var ApiContext
     */
    private $apiContext;

    public function __construct(ContainerInterface $container, ApiContext $apiContext)
    {
        $this->container = $container;
        $this->apiContext = $apiContext;
    }

    /**
     * @param array $data
     * @param bool $encodePassword
     * @return User
     * @throws \App\Model\Api\ApiException
     */
    public function createNewUser(array $data, bool $encodePassword = true) {
        $user = new User();
        $this->createNewAbstractUser($user, $data, $encodePassword);

        return $user;
    }

    /**
     * @param User $user
     * @param array $data
     * @param bool $encodePassword
     * @return User
     * @throws \App\Model\Api\ApiException
     */
    public function createNewAbstractUser(User $user, array $data, bool $encodePassword = true) {
        $user->setEmail($data['email']);
        $user->setPassport($data['passport']);
        $user->setVkontakte($data['vkId']??null);
        $user->setFaceBook($data['faceBookId']??null);
        $user->setGoogle($data['googleId']??null);
        $user->setRoles($data['roles']);
        if(!in_array('ROLE_USER', $user->getRoles())){
            $user->addRole('ROLE_USER');
        }
        if($encodePassword) {
            $password = $this->encodePlainPassword($data['password']);
        } else {
            $password = $data['password'];
        }

        $user->setPassword($password);

        return $user;
    }

    /**
     * @param array $data
     * @param bool $encodePassword
     * @return User
     * @throws \App\Model\Api\ApiException
     */
    public function createNewTenant(array $data, bool $encodePassword = true) {
        /** @var Tenant $user */
        $user = $this->createNewAbstractUser(new Tenant(), $data, $encodePassword);
        $user->setCallName($data['call_name']??null);

        return $user;
    }

    /**
     * @param array $data
     * @param bool $encodePassword
     * @return User
     * @throws \App\Model\Api\ApiException
     */
    public function createNewLandlord(array $data, bool $encodePassword = true) {
        /** @var Landlord $user */
        $user = $this->createNewAbstractUser(new Landlord(), $data, $encodePassword);
        $user->setFullName($data['full_name']??null);

        return $user;
    }


    /**
     * @param string $password
     * @return string
     * @throws \App\Model\Api\ApiException
     */
    public function encodePlainPassword(string $password): string
    {
        return $this->apiContext->encodePassword($password);
    }

    /**
     * @param User $user
     * @param array $social_info
     * @return User $user
     */

    /**
     * @param User $user
     */
    public function putUserInSession($user) {
        $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
        $this->container->get('security.token_storage')->setToken($token);
        $this->container->get('session')->set('_security_main', serialize($token));
    }

    /**
     * @param User $user
     * @return array
     */
    public function getSocialInfo($user) {
        return [
            'vkontakte' => ($user->getVkontakte() != null) ? true : null,
            'google' => ($user->getGoogle() != null) ? true : null,
            'facebook' => ($user->getFacebook() != null) ? true : null,
        ];
    }
}
