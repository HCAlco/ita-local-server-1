<?php

namespace App\DataFixtures;

use App\Model\User\UserHandler;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class UserFixtures extends Fixture
{
    private $userHandler;

    public function __construct(UserHandler $userHandler)
    {
        $this->userHandler = $userHandler;
    }

    /**
     * @param ObjectManager $manager
     * @throws \App\Model\Api\ApiException
     */
    public function load(ObjectManager $manager)
    {

        $user = $this->userHandler->createNewUser([
            'email' => 'tenant',
            'passport' => 'tenantpassport',
            'password' => '81dc9bdb52d04dc20036dbd8313ed055be041b21f66931f5a1d24e1e19a78539',//1234
            'roles' => ["ROLE_TENANT"]
        ], false);

        $user1 = $this->userHandler->createNewUser([
            'email' => 'landlord',
            'passport' => 'landlordpassport',
            'password' => '81dc9bdb52d04dc20036dbd8313ed055be041b21f66931f5a1d24e1e19a78539',//1234
            'roles' => ["ROLE_TENANT"]
        ], false);

        $manager->persist($user);
        $manager->persist($user1);
        $manager->flush();
    }
}
