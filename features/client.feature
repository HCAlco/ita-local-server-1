# language: ru

Функционал: Тестируем функционал сайта по регистрации пользователей

  Сценарий: Проверка регистрации и авторизации арендатора
    Допустим я нахожусь на странице регистрации пользователей
    И я регистрируюсь со следующими данными email:"tenant1" passport: "tenant1" password: "1234" role: "tenant"
    И я нахожусь на странице авторизации пользователей
    И я вижу слово "Логин" где-то на странице
    И я авторизуюсь со следующими данными email:"tenant1" password: "1234"
    И я вижу слово "Мы" где-то на странице
    И я захожу на страницу своего профиля

    # ДЛЯ КОРРЕКТНОЙ РАБОТЫ НЕОБХОДИМО ВКЛЮЧИТЬ ОБА СЕРВЕРА И ЗАПУСТИТЬ ФИКСТУРЫ НА ЦЕНТРАЛЬНОМ СЕРВЕРЕ, А ТАКЖЕ ВЗЯТЬ ТОКЕН С ЦЕНТРАЛЬНОГО
    # ДЛЯ КОРРЕКТНОЙ РАБОТЫ НЕОБХОДИМО ВКЛЮЧИТЬ ОБА СЕРВЕРА И ЗАПУСТИТЬ ФИКСТУРЫ НА ЦЕНТРАЛЬНОМ СЕРВЕРЕ, А ТАКЖЕ ВЗЯТЬ ТОКЕН С ЦЕНТРАЛЬНОГО

  Сценарий: Проверка регистрации и авторизации арендодателя
    Допустим я нахожусь на странице регистрации пользователей
    И я регистрируюсь со следующими данными email:"landlord1" passport: "landlord1" password: "1234" role: "landlord"
    И я нахожусь на странице авторизации пользователей
    И я вижу слово "Логин" где-то на странице
    И я авторизуюсь со следующими данными email:"landlord1" password: "1234"
    И я вижу слово "Мы" где-то на странице
    И я захожу на страницу своего профиля

    Сценарий: Проверка валидации данных при регистрации пользователя с существующим именем
    Допустим я нахожусь на странице регистрации пользователей
    И я регистрируюсь со следующими данными email:"landlord1" passport: "landlord52" password: "1234" role: "landlord"
    И я вижу слово "Клиент с таким именем уже существует. Перейдите на страницу авторизации" где-то на странице
