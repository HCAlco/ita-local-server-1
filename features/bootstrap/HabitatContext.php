<?php

/**
 * Defines application features from the specific context.
 */
class HabitatContext extends AttractorContext
{
    /**
     * @When /^я регистрирую объект со следующими данными type:"([^"]*)" title: "([^"]*)" roomqty: "([^"]*)" phone: "([^"]*)" x:"([^"]*)" y:"([^"]*)" price: "([^"]*)"$/
     * @param $type
     * @param $title
     * @param $roomqty
     * @param $phone
     * @param $price
     */
    public function яРегистрируюОбъектСоСледующимиДанными($type, $title, $roomqty, $phone, $x, $y,$price)
    {
        $this->selectOption('habitat_register_type', $type);
        $this->getSession()->executeScript(';
            $(function initt() {
                myPlacemark = new ymaps.Placemark(['.$x.', '.$y.']);
                myMap.geoObjects.add(myPlacemark);
                $(\'#habitat_register_address\').attr(\'value\', \''.$x.', '.$y.'\');
            })');
        $this->fillField('habitat_register_title', $title);
        $this->fillField('habitat_register_roomqty', $roomqty);
        $this->fillField('habitat_register_phone', $phone);
        $this->fillField('habitat_register_price', $price);
        $this->pressButton('habitat_register_save');
        if($type == 'pension'){
            $this->fillField('pension_register_restaurants', 1);
            $this->fillField('pension_register_slippers', 1);
            $this->pressButton('pension_register_save');
        }else{
            $this->fillField('cottage_register_bathrooms', 2);
            $this->fillField('cottage_register_bathrobes', 2);
            $this->pressButton('cottage_register_save');
        }
    }

    /**
     * @When /^я ищу объекты с данными title: "([^"]*)" min: "([^"]*)" max: "([^"]*)" type: "([^"]*)"$/
     * @param $title
     * @param $min
     * @param $max
     * @param $type
     */
    public function яИщуОбъектыСданными($title, $min, $max, $type)
    {
        if(!empty($title)){ $this->fillField('filter_title', $title); }
        if(!empty($min)){ $this->fillField('filter_min', $min); }
        if(!empty($max)){ $this->fillField('filter_max', $max); }
        if(!empty($type)){ $this->selectOption('filter_type', $type); }
        $this->pressButton('filter_save');
    }


    /**
     * @When /^я захожу на страницу регистрации объекта$/
     */
    public function яЗахожуНаСтраницуРегистрацииОбъекта()
    {
        $this->visit($this->getContainer()->get('router')->generate('app_habitat_register'));
    }

    /**
     * @When /^я захожу на страницу объекта "([^"]*)"$/
     */
    public function яЗахожуНаСтраницуОбъекта($habitat)
    {
        $this->clickLink($habitat);
    }

    /**
     * @When /^я нажимаю на кнопку "([^"]*)"$/
     */
    public function яНажимаюНаКнопку($button)
    {
        $this->pressButton($button);
    }



}

