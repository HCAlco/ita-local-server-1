# language: ru

Функционал: Тестируем функционал сайта по регистрации объектов и их поиску

  Сценарий: Проверка регистрации пансионата
    Допустим я нахожусь на странице авторизации пользователей
    И я авторизуюсь со следующими данными email:"landlord1" password: "1234"
    И я захожу на страницу регистрации объекта
    И я регистрирую объект со следующими данными type:"pension" title: "Пансионат" roomqty: "12" phone: "0555 321 123" x:"42.641975" y:"77.182632" price: "400"
    И я вижу слово "Пансионат" где-то на странице

    # ДЛЯ КОРРЕКТНОЙ РАБОТЫ НЕОБХОДИМО ВКЛЮЧИТЬ ОБА СЕРВЕРА И ЗАПУСТИТЬ ФИКСТУРЫ НА ЦЕНТРАЛЬНОМ СЕРВЕРЕ, А ТАКЖЕ ВЗЯТЬ ТОКЕН С ЦЕНТРАЛЬНОГО
    # ДЛЯ КОРРЕКТНОЙ РАБОТЫ НЕОБХОДИМО ВКЛЮЧИТЬ ОБА СЕРВЕРА И ЗАПУСТИТЬ ФИКСТУРЫ НА ЦЕНТРАЛЬНОМ СЕРВЕРЕ, А ТАКЖЕ ВЗЯТЬ ТОКЕН С ЦЕНТРАЛЬНОГО
  Сценарий: Проверка регистрации коттеджа
    Допустим я нахожусь на странице авторизации пользователей
    И я авторизуюсь со следующими данными email:"landlord1" password: "1234"
    И я захожу на страницу регистрации объекта
    И я регистрирую объект со следующими данными type:"cottage" title: "Коттедж" roomqty: "6" phone: "0555 123 321" x:"42.64200" y:"77.182333" price: "600"
    И я вижу слово "Коттедж" где-то на странице


  Сценарий: Проверка поиска по имени
    Допустим я нахожусь на главной странице
    И я ищу объекты с данными title: "" min: "" max: "500" type: ""

  Сценарий: Проверка поиска по максимальной цене
    Допустим я нахожусь на главной странице
    И я ищу объекты с данными title: "" min: "" max: "" type: "pension"

  Сценарий: Проверка ошибки при бронировании без авторизации
    Допустим я нахожусь на главной странице
    И я захожу на страницу объекта "Коттедж"
    И я вижу слово "Забронировать" где-то на странице
    И я нажимаю на кнопку "Забронировать"
    И я вижу слово "Авторизуйтесь как арендатор, чтобы бронировать" где-то на странице
    Допустим я нахожусь на главной странице
    И я ищу объекты с данными title: "Коттедж" min: "" max: "" type: ""

  Сценарий: Проверка поиска по минимальной цене
    Допустим я нахожусь на главной странице
    И я ищу объекты с данными title: "" min: "500" max: "" type: ""

  Сценарий: Проверка поиска по максимальной цене

  Сценарий: Проверка ошибки при бронировании арендодателем
    Допустим я нахожусь на странице авторизации пользователей
    И я авторизуюсь со следующими данными email:"landlord1" password: "1234"
    И я захожу на страницу объекта "Коттедж"
    И я вижу слово "Забронировать" где-то на странице
    И я нажимаю на кнопку "Забронировать"
    И я вижу слово "Авторизуйтесь как арендатор, чтобы бронировать" где-то на странице

  Сценарий: Проверка бронирования
    Допустим я нахожусь на странице авторизации пользователей
    И я авторизуюсь со следующими данными email:"tenant1" password: "1234"
    И я захожу на страницу объекта "Коттедж"
    И я нажимаю на кнопку "Забронировать"